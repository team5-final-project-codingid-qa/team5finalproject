<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>2-MOBILE</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>1804412b-ce72-4526-bd16-e9e2f0f98867</testSuiteGuid>
   <testCaseLink>
      <guid>942e9751-8b1f-4948-aed8-faa1fb98aa03</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Dany/Mobile/Register/Positive/TCM-01 Buat akun baru sesuai format</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>e08cde42-ef15-4b17-bbbf-fad616a03bb0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Dany/Mobile/Register/Negative/TCM-09 Buat akun dengan nomor telepon 4 digit</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>ab22d697-0cf7-4769-a17d-299d1d87c6dc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Dany/Mobile/Register/Negative/TCM-15 Buat akun dengan password dan confirm password berbeda</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>82df7a7c-c5a4-451a-a59c-20b95b2ab8e1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Dany/Mobile/Register/Negative/TCM-18 Buat akun dengan email yang sudah digunakan akun lain</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>37541710-3312-4521-a9c8-841ac961a963</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/Mobile/Login/Positive/TCM-21 Login Akun valid telah melakukan verifikasi</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>9b8a7872-4ff3-4675-8a38-066e9f649a2d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/Mobile/Login/Negative/TCM-22 Login dengan Akun yang belum melakukan verifikasi</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>f27b0619-4d73-43d4-8945-7f684dc40928</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/Mobile/Login/Negative/TCM-23 Login dengan Email dan Password Kosong</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>687e5187-f88a-46b0-b8de-c1d7b20a1aa7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/Mobile/Profile/Positive/TCM-32 Mengubah Profile User Fullname saja</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>6a09f38d-fb7a-43ad-8649-30624aedcf9a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/Mobile/Profile/Negative/TCM-34 Mengubah Profile User Fullname menjadi kosong</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>3c69e54c-a086-463e-a7ec-67d7ba16f4ec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/Mobile/Profile/Positive/TCM-35 Mengubah Profile User Phone saja</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>9ff13650-7efb-42b5-a52a-4ff3e056021a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/Mobile/Profile/Negative/TCM-36 Mengubah Profile User Phone lebih dari 13 digit (masukan 15 digit)</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>2e875ee7-5c64-40b5-888a-dbbe1c0817d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/Mobile/Profile/Negative/TCM-37 Mengubah Profile User Phone kurang dari 9 digit (masukkan 8 digit) (Failed TC)</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>b3ca5035-0167-4a9a-bcc5-31d70da3d725</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/Mobile/Profile/Positive/TCM-40 Mengubah Profile User pada BirthDay saja</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>1138f0aa-3384-46a5-b8f1-80960ad5ff23</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/Mobile/Profile/Negative/TCM-42 Mengubah Profil User BirthDay menjadi kosong (mengkosongkan isi BirthDay)</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>71bec709-5cd6-4a2d-be36-d3d91c6bb162</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Dany/Mobile/Change Profile/Negative/TCM-43 Edit profil tanpa mengubah apapun</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>e18038d0-8e3b-490e-9883-31381d50c287</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Dany/Mobile/Change Profile/Negative/TCM-47 Tolak perubahan edit profil</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>e46264c8-6c17-4843-8b2b-0256c4ac17d1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Dany/Mobile/Change Profile/Positive/TCM-48 Ubah kata sandi dengan kata sandi lama yang benar dan kata sandi baru memenuhi syarat password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>642099bd-a421-4d8f-9fec-659d707e1eb4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Dany/Mobile/Change Profile/Negative/TCM-51 Ubah kata sandi dengan kata sandi hanya 7 karakter</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>47360f27-34be-425c-95a5-ca4698950907</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Dany/Mobile/Change Profile/Negative/TCM-55 Ubah kata sandi dengan kata sandi baru di field password dan confirm beda</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
