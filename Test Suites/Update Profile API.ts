<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Update Profile API</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>201895be-2496-43c0-8267-7e88dd8be0d2</testSuiteGuid>
   <testCaseLink>
      <guid>398ab833-bf7a-409f-8cfa-c6f1f14eaaa5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/API/Update Profile/Positive/TCA-08 Update Profile hanya Name saja</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>60ca442e-5e24-4464-b0d7-7a59c8e7bb63</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/API/Update Profile/Positive/TCA-09 Update Profile hanya WhatsApp saja</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>c5728a22-97e4-4730-8e52-873fd2d6d38e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/API/Update Profile/Negative/TCA-10 Update Profile Name dengan menggunakan number (Failed TC)</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>4e2bcb5a-6f14-4105-91da-7493e7258575</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/API/Update Profile/Negative/TCA-11 Update Profile Name dengan menggunakan simbol (Failed TC)</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>a6f289ad-0b2d-44c9-a36e-a8ea9cfc6d16</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/API/Update Profile/Negative/TCA-12 Update Profile WhatsApp dengan Alphabet (Failed TC)</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>26aa9980-296a-4aec-89e1-43e2a112c53b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/API/Update Profile/Negative/TCA-13 Update Profile WhatsApp dengan Simbol (Failed TC)</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
