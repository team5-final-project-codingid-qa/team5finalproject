<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Change Profile Website</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>faa54a87-c5d5-4801-a431-ed80c1e2a4fc</testSuiteGuid>
   <testCaseLink>
      <guid>1350156d-bab9-4270-b254-a7a8b8012009</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/Website/Change Profile/Positive/TCW-32 Change Profile user Fullname saja</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>3b8fce33-f298-47c8-94f7-1aaec1264ff2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/Website/Change Profile/Negative/TCW-33 Change Profile user Fullname dengan karakter number (Failed TC)</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>519fe812-65b6-4ee1-b912-2876f3c50d58</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/Website/Change Profile/Negative/TCW-35 Change Profile user Fullname dengan mengkosongkan (menghapus Fullname)</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>90437c4e-c9e3-4eda-91cc-86f6a0c359a6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/Website/Change Profile/Negative/TCW-36 Change Profile user Password dengan karakter simbol (Failed TC)</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>f7aa7c94-d2f0-4e61-b5ac-e8c8323ae9b8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/Website/Change Profile/Negative/TCW- Change Profile user Password dengan password awal</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
