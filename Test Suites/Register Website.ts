<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Register Website</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>4f3ce144-4cd6-473b-830f-dc7fa8f22844</testSuiteGuid>
   <testCaseLink>
      <guid>cab5227e-1298-47f2-84be-7dc1f77805e3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/Website/Register/Positive/TCW-01 Registrasi Akun Baru</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>f1aca4bb-536c-45cd-93fa-c972f48c5af5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/Website/Register/Negative/TCW-02 Registrasi Akun Baru dengan Email yang terdaftar</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>f5954711-4289-4fe9-9d97-a4eb7cd4d5ee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/Website/Register/Positive/TCW-03 Registrasi Akun Baru dengan user berumur 6 tahun (Failed TC)</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>31c66c71-25cd-4111-88af-8951f1b87762</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/Website/Register/Negative/TCW-04 Registrasi Akun Baru dengan Nama karakter number (Failed TC)</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>e039c063-bb84-4015-ae2c-502ec97db0f4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/Website/Register/Negative/TCW-05 Registrasi Akun Baru tanpa simbol (at) pada Email</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>34b49786-c3b2-437b-8ea7-394c2612bc73</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/Website/Register/Negative/TCW-06 Registrasi Akun Baru tanpa simbol (.) pada Email (Failed TC)</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>27de660a-07da-4c18-8d2c-60434b9e5842</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/Website/Register/Negative/TCW-07 Registrasi Akun Baru dengan WhatsApp lebih dari 12 digit (Failed TC)</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>e616530d-20be-4282-925b-c775166339a0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/Website/Register/Negative/TCW-08 Registrasi Akun Baru dengan WhatsApp kurang dari 10 digit (Failed TC)</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>de68166e-918c-4706-b398-fd4d7ce390b3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/Website/Register/Negative/TCW-09 Registrasi Akun Baru dengan Kata Sandi kurang dari 8 karakter</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>6fd39d86-ba92-488a-bc00-aa8ee19959c4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/Website/Register/Negative/TCW-10 Registrasi Akun Baru dengan Kata Sandi semua karakter menggunakan simbol (Failed TC)</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>68905610-1377-47ac-9856-04f068ac632a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/Website/Register/Negative/TCW-11 Registrasi Akun Baru dengan Kata Sandi berbeda dengan Konfirmasi Sandi</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>f5694e3f-12f9-4cc4-9613-a724053e598d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/Website/Register/Negative/TCW-12 Registrasi Akun Baru tanpa menchecklist Syarat dan ketentuan</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>6f6baaec-f341-4d10-bf8d-83ecf416dcd0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/Website/Register/Negative/TCW-13 Registrasi Akun Baru dengan mengkosongkan Nama</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>5992ce7f-4b4b-4a02-a49d-c889d4764a41</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/Website/Register/Negative/TCW-14 Registrasi Akun Baru dengan mengkosongkan Tanggal Lahir</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>c09702b2-6503-442d-b1e5-67d89d257e86</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/Website/Register/Negative/TCW-15 Registrasi Akun Baru dengan mengkosongkan Email</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>1aec5822-936f-4b6b-9862-c6159cbe7545</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/Website/Register/Negative/TCW-16 Registrasi Akun Baru dengan mengkosongkan WhatsApp</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>83a49ece-2fa3-47f3-8ace-2bd245ff0249</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/Website/Register/Negative/TCW-17 Registrasi Akun Baru dengan mengkosongkan Kata Sandi</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>88a40290-e522-42bb-a985-23ecc06fcfb0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Ardel/Website/Register/Negative/TCW-18 Registrasi Akun Baru dengan mengkosongkan Konfirmasi Kata Sandi</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
