<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>mobile_change_password</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>03021c56-b136-4379-81df-e62d3ce8861b</testSuiteGuid>
   <testCaseLink>
      <guid>ea16ca77-a037-4fd0-bf67-9d8958f4ef0e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Dany/Mobile/Change Profile/Positive/TCM-48 Ubah kata sandi dengan kata sandi lama yang benar dan kata sandi baru memenuhi syarat password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>6f516256-095e-4420-8338-a1cbae13fe68</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Dany/Mobile/Change Profile/Negative/TCM-49 Ubah kata sandi dengan kata sandi lama kosong</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>23a009d9-88b2-4390-a623-359aaae7ceac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Dany/Mobile/Change Profile/Negative/TCM-51 Ubah kata sandi dengan kata sandi hanya 7 karakter</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>00e6fa26-0df7-4ef0-93e0-9a49743de2b2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Case Punya Dany/Mobile/Change Profile/Negative/TCM-55 Ubah kata sandi dengan kata sandi baru di field password dan confirm beda</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
