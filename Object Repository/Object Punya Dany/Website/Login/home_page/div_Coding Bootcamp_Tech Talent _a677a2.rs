<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Coding Bootcamp_Tech Talent _a677a2</name>
   <tag></tag>
   <elementGuidId>3c3817c6-c2d0-412b-a1c4-2ac6d8f9aa63</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-body-homepage']/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.containerHeader</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>1d045b8e-3b57-46ae-a4d7-d19fa62bab5d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>containerHeader</value>
      <webElementGuid>b4f186b4-c6d1-4dce-8b63-bb57daea878a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x-data</name>
      <type>Main</type>
      <value>{}</value>
      <webElementGuid>e4d91e92-0400-4ff3-94e8-8b2c73920acc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        Coding Bootcamp
            Tech Talent Berkualitas

        Bootcamp untuk semua,
            dengan
            background IT maupun Non-IT.
            Pilih programnya dan dapatkan jaminan kerja.
                    
                
                    
                    
                        Quality Assurance Engineer Class
                                                    
                                Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                                            
                            
                                Daftar Sekarang
                            
                        
                                                
                    
                
                    
                    
                        Fullstack Engineer Class
                                                    
                                Jadi Fullstack Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                                            
                            
                                Daftar Sekarang
                            
                        
                                                
                



        
            
                
                    Belum kerja?
                        Tidak usah bayar!
                    Jaminan uang kembali 100% jika kamu tidak
                        mendapatkan
                        pekerjaan untuk Upfront Payment dan tidak perlu
                        membayar sampai kamu dapat kerja dengan metode ISA
                

                
                    Belajar dari Praktisi
                    Coach di Coding.ID merupakan praktisi yang telah
                        berpengalaman di bidangnya loh jadi tenang aja kamu berada di tangan yang tepat kok
                

                
                    Akses ke semua
                        Event &amp; Course
                    Peserta maupun alumni akan mendapatkan privilage
                        untuk
                        mengakses semua semua Events maupun Online Course di Coding.ID secara gratis!
                

                
                    Belajar dari Nol sampai Jago
                    Kamu masih awam? Tenang aja Coding.ID akan
                        membangun
                        kemampuan programing kamu secara mandiri dengan cara-cara yang sangat sederhana.
                
            


            
                
                    
                
                
                    
                
                
                    
                
                
                    
                
            

        

    </value>
      <webElementGuid>b5172c9c-60e6-4d30-9f9e-52427c2f7680</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body-homepage&quot;)/div[@class=&quot;containerHeader&quot;]</value>
      <webElementGuid>497ad7b1-3091-4e61-8cdb-11d6f0ee77e8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div</value>
      <webElementGuid>710c643b-9a00-481b-bd16-5902251ce808</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[4]/div</value>
      <webElementGuid>ad00fc39-c76d-406c-b8be-0867d252c47a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        Coding Bootcamp
            Tech Talent Berkualitas

        Bootcamp untuk semua,
            dengan
            background IT maupun Non-IT.
            Pilih programnya dan dapatkan jaminan kerja.
                    
                
                    
                    
                        Quality Assurance Engineer Class
                                                    
                                Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                                            
                            
                                Daftar Sekarang
                            
                        
                                                
                    
                
                    
                    
                        Fullstack Engineer Class
                                                    
                                Jadi Fullstack Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                                            
                            
                                Daftar Sekarang
                            
                        
                                                
                



        
            
                
                    Belum kerja?
                        Tidak usah bayar!
                    Jaminan uang kembali 100% jika kamu tidak
                        mendapatkan
                        pekerjaan untuk Upfront Payment dan tidak perlu
                        membayar sampai kamu dapat kerja dengan metode ISA
                

                
                    Belajar dari Praktisi
                    Coach di Coding.ID merupakan praktisi yang telah
                        berpengalaman di bidangnya loh jadi tenang aja kamu berada di tangan yang tepat kok
                

                
                    Akses ke semua
                        Event &amp; Course
                    Peserta maupun alumni akan mendapatkan privilage
                        untuk
                        mengakses semua semua Events maupun Online Course di Coding.ID secara gratis!
                

                
                    Belajar dari Nol sampai Jago
                    Kamu masih awam? Tenang aja Coding.ID akan
                        membangun
                        kemampuan programing kamu secara mandiri dengan cara-cara yang sangat sederhana.
                
            


            
                
                    
                
                
                    
                
                
                    
                
                
                    
                
            

        

    ' or . = '
        Coding Bootcamp
            Tech Talent Berkualitas

        Bootcamp untuk semua,
            dengan
            background IT maupun Non-IT.
            Pilih programnya dan dapatkan jaminan kerja.
                    
                
                    
                    
                        Quality Assurance Engineer Class
                                                    
                                Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                                            
                            
                                Daftar Sekarang
                            
                        
                                                
                    
                
                    
                    
                        Fullstack Engineer Class
                                                    
                                Jadi Fullstack Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                                            
                            
                                Daftar Sekarang
                            
                        
                                                
                



        
            
                
                    Belum kerja?
                        Tidak usah bayar!
                    Jaminan uang kembali 100% jika kamu tidak
                        mendapatkan
                        pekerjaan untuk Upfront Payment dan tidak perlu
                        membayar sampai kamu dapat kerja dengan metode ISA
                

                
                    Belajar dari Praktisi
                    Coach di Coding.ID merupakan praktisi yang telah
                        berpengalaman di bidangnya loh jadi tenang aja kamu berada di tangan yang tepat kok
                

                
                    Akses ke semua
                        Event &amp; Course
                    Peserta maupun alumni akan mendapatkan privilage
                        untuk
                        mengakses semua semua Events maupun Online Course di Coding.ID secara gratis!
                

                
                    Belajar dari Nol sampai Jago
                    Kamu masih awam? Tenang aja Coding.ID akan
                        membangun
                        kemampuan programing kamu secara mandiri dengan cara-cara yang sangat sederhana.
                
            


            
                
                    
                
                
                    
                
                
                    
                
                
                    
                
            

        

    ')]</value>
      <webElementGuid>ff23bab7-3105-4e76-adb4-dd9894068ac8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        Coding Bootcamp
            Tech Talent Berkualitas

        Bootcamp untuk semua,
            dengan
            background IT maupun Non-IT.
            Pilih programnya dan dapatkan jaminan kerja.
                    
                
                    
                    
                        Quality Assurance Engineer Class
                                                    
                                Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                    
                        Daftar
                            Sekarang
                    
                            
                    
                
                    
                    
                        Fullstack Engineer Class
                                                    
                                Jadi Fullstack Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                    
                        Daftar
                            Sekarang
                    
                            
                



        
            
                
                    Belum kerja?
                        Tidak usah bayar!
                    Jaminan uang kembali 100% jika kamu tidak
                        mendapatkan
                        pekerjaan untuk Upfront Payment dan tidak perlu
                        membayar sampai kamu dapat kerja dengan metode ISA
                

                
                    Belajar dari Praktisi
                    Coach di Coding.ID merupakan praktisi yang telah
                        berpengalaman di bidangnya loh jadi tenang aja kamu berada di tangan yang tepat kok
                

                
                    Akses ke semua
                        Event &amp; Course
                    Peserta maupun alumni akan mendapatkan privilage
                        untuk
                        mengakses semua semua Events maupun Online Course di Coding.ID secara gratis!
                

                
                    Belajar dari Nol sampai Jago
                    Kamu masih awam? Tenang aja Coding.ID akan
                        membangun
                        kemampuan programing kamu secara mandiri dengan cara-cara yang sangat sederhana.
                
            


            
                
                    
                
                
                    
                
                
                    
                
                
                    
                
            

        

    ' or . = '
        Coding Bootcamp
            Tech Talent Berkualitas

        Bootcamp untuk semua,
            dengan
            background IT maupun Non-IT.
            Pilih programnya dan dapatkan jaminan kerja.
                    
                
                    
                    
                        Quality Assurance Engineer Class
                                                    
                                Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                    
                        Daftar
                            Sekarang
                    
                            
                    
                
                    
                    
                        Fullstack Engineer Class
                                                    
                                Jadi Fullstack Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.
                                            

                
                                    
                        Daftar
                            Sekarang
                    
                            
                



        
            
                
                    Belum kerja?
                        Tidak usah bayar!
                    Jaminan uang kembali 100% jika kamu tidak
                        mendapatkan
                        pekerjaan untuk Upfront Payment dan tidak perlu
                        membayar sampai kamu dapat kerja dengan metode ISA
                

                
                    Belajar dari Praktisi
                    Coach di Coding.ID merupakan praktisi yang telah
                        berpengalaman di bidangnya loh jadi tenang aja kamu berada di tangan yang tepat kok
                

                
                    Akses ke semua
                        Event &amp; Course
                    Peserta maupun alumni akan mendapatkan privilage
                        untuk
                        mengakses semua semua Events maupun Online Course di Coding.ID secara gratis!
                

                
                    Belajar dari Nol sampai Jago
                    Kamu masih awam? Tenang aja Coding.ID akan
                        membangun
                        kemampuan programing kamu secara mandiri dengan cara-cara yang sangat sederhana.
                
            


            
                
                    
                
                
                    
                
                
                    
                
                
                    
                
            

        

    ')]</value>
      <webElementGuid>5fa8a1a2-94a5-4588-97f7-3abadacbe44b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
