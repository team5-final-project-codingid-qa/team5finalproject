import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration

Mobile.startApplication(RunConfiguration.getProjectDir() + '/Apk/DemoAppV2.apk', true)

Mobile.tap(findTestObject('Object Repository/Object Punya Dany/Mobile/Change Profile and Password/bottom_profile_c'), 0)

Mobile.tap(findTestObject('Object Repository/Object Punya Dany/Mobile/Change Profile and Password/profile_login_button_loggedout'), 
    0)

Mobile.setText(findTestObject('Object Repository/Object Punya Dany/Mobile/Change Profile and Password/login_email'), 'fmdemo_app@mailinator.com', 
    0)

Mobile.setText(findTestObject('Object Repository/Object Punya Dany/Mobile/Change Profile and Password/login_password'), 
    'dem0pa55!', 0)

Mobile.tap(findTestObject('Object Repository/Object Punya Dany/Mobile/Change Profile and Password/login_login_btn'), 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Object Repository/Object Punya Dany/Mobile/Change Profile and Password/bottom_profile_c'), 0)

Mobile.tap(findTestObject('Object Repository/Object Punya Dany/Mobile/Change Profile and Password/profile_gear_icon'), 0)

Mobile.tap(findTestObject('Object Repository/Object Punya Dany/Mobile/Change Profile and Password/profile_gear_editprofile'), 
    0)

Mobile.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('Object Repository/Object Punya Dany/Mobile/Change Profile and Password/edit_profile_img_button'), 
    0)

Mobile.tap(findTestObject('Object Repository/Object Punya Dany/Mobile/Change Profile and Password/edit_profile_imgmodal_camera'), 
    0)

Mobile.tap(findTestObject('Object Repository/Object Punya Dany/Mobile/Change Profile and Password/android_camera_capture'), 
    0)

Mobile.tap(findTestObject('Object Punya Dany/Mobile/Change Profile and Password/android_camera_ok'), 0)

Mobile.tap(findTestObject('Object Repository/Object Punya Dany/Mobile/Change Profile and Password/cropper_ok'), 0)

Mobile.delay(15, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.verifyElementAttributeValue(findTestObject('Object Repository/Object Punya Dany/Mobile/Change Profile and Password/edit_profile_btn_save'), 
    'enabled', 'true', 0)

Mobile.closeApplication()

