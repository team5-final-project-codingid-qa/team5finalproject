import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration

Mobile.startApplication(RunConfiguration.getProjectDir() + '/Apk/DemoAppV2.apk', true)

Mobile.tap(findTestObject('Object Repository/Object Punya Dany/Mobile/Register/main_login_button'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Object Repository/Object Punya Dany/Mobile/Register/login_register_now'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('Object Punya Dany/Mobile/Register/register_name'), 'Auto Demo', 0, FailureHandling.STOP_ON_FAILURE)

//date picker handling
Mobile.tap(findTestObject('Object Punya Dany/Mobile/Register/register_dob_button'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Object Repository/Object Punya Dany/Mobile/Register/android_date_selector_current_year'), 0, 
    FailureHandling.STOP_ON_FAILURE)

//Mobile.swipe(200, 520, 200, 1155)

Mobile.swipe(200, 1520, 1240, 1520, FailureHandling.STOP_ON_FAILURE)

Mobile.scrollToText('2004', FailureHandling.STOP_ON_FAILURE)

Mobile.scrollToText('2000', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Object Repository/Object Punya Dany/Mobile/Register/android_date_selector_year_select_2000'), 
    0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Object Repository/Object Punya Dany/Mobile/Register/android_date_day_1'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Object Punya Dany/Mobile/Register/android_date_selector_ok'), 0, FailureHandling.STOP_ON_FAILURE)

//end of date picker handling
Mobile.setText(findTestObject('Object Punya Dany/Mobile/Register/register_email'), 'fmdemo_app2@mailinator.com', 0, FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('Object Punya Dany/Mobile/Register/register_phone'), '081000000', 0, FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('Object Punya Dany/Mobile/Register/register_password'), 'dem0pa55!', 0, FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('Object Punya Dany/Mobile/Register/register_password2'), 'dem0pa55!', 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Object Punya Dany/Mobile/Register/register_terms'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementAttributeValue(findTestObject('Object Punya Dany/Mobile/Register/register_finish_button'), 'enabled', 
    'true', 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Object Punya Dany/Mobile/Register/register_finish_button'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('Object Punya Dany/Mobile/Register/register_after_header'), 0, FailureHandling.STOP_ON_FAILURE)

