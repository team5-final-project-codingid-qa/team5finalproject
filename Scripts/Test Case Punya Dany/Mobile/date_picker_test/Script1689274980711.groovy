import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration

Mobile.startApplication(RunConfiguration.getProjectDir() + '/Apk/DemoAppV2.apk', true)

Mobile.tap(findTestObject('Object Punya Dany/Mobile/Register/main_login_button'), 0)

Mobile.tap(findTestObject('Object Punya Dany/Mobile/Register/login_register_now'), 0)

Mobile.tap(findTestObject('Object Punya Dany/Mobile/Register/register_dob_button'), 0)

Mobile.tap(findTestObject('Object Repository/Object Punya Dany/Mobile/Register/android_date_selector_current_year'), 
    0)

Mobile.swipe(200, 520, 200, 1000)

Mobile.tap(findTestObject('Object Repository/Object Punya Dany/Mobile/Register/android_date_selector_year_select_2000'), 
    0)

Mobile.tap(findTestObject('Object Repository/Object Punya Dany/Mobile/Register/android_date_day_1'), 0)

Mobile.tap(findTestObject('Object Punya Dany/Mobile/Register/android_date_selector_ok'), 0)

Mobile.closeApplication()

