import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Test Case Punya Ardel/Website/Login/Login akun valid ardel'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Punya Ardel/Website/Page_Coding.ID/homePageAfterLogin'))

WebUI.click(findTestObject('Object Punya Ardel/Website/Page_Coding.ID/buttonMyAccount'))

WebUI.click(findTestObject('Object Punya Ardel/Website/PageDashboardUserProfile/buttonProfil'))

WebUI.click(findTestObject('Object Punya Ardel/Website/PageDashboardUserProfile/buttonEditProfile'))

WebUI.setText(findTestObject('Object Punya Ardel/Website/PageDashboardUserProfile/inputFullname'), '1234567')

WebUI.click(findTestObject('Object Punya Ardel/Website/PageDashboardUserProfile/buttonSaveChanges'))

WebUI.verifyElementText(findTestObject('Object Punya Ardel/Website/PageDashboardUserProfile/alertChangeProfile'), 'Fullname must be Aplhabet.')

