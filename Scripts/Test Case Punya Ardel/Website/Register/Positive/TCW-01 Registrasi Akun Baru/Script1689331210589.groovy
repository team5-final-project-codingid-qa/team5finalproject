import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('https://demo-app.online/')

WebUI.click(findTestObject('Object Punya Ardel/Website/Page_Coding.ID/buttonBuatAkun'))

WebUI.setText(findTestObject('Object Punya Ardel/Website/PageBuatAkun/inputNama'), 'Gojo satoru')

WebUI.setText(findTestObject('Object Punya Ardel/Website/PageBuatAkun/inputTanggalLahir'), '1-Jan-2000')

WebUI.setText(findTestObject('Object Punya Ardel/Website/PageBuatAkun/inputEmail'), 'gojosatoru@mailinator.com')

WebUI.setText(findTestObject('Object Punya Ardel/Website/PageBuatAkun/inputWhatsapp'), '1234567890')

WebUI.setEncryptedText(findTestObject('Object Punya Ardel/Website/PageBuatAkun/inputKataSandi'), 'idOJX1S4S7QVt6o9Fe9nQg==')

WebUI.setEncryptedText(findTestObject('Object Punya Ardel/Website/PageBuatAkun/inputKonfirmasiSandi'), 'idOJX1S4S7QVt6o9Fe9nQg==')

WebUI.click(findTestObject('Object Punya Ardel/Website/PageBuatAkun/inputCheckListSyaratKetentuan'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Punya Ardel/Website/PageBuatAkun/buttonDaftar'))

WebUI.verifyElementVisible(findTestObject('Object Punya Ardel/Website/PageVerifikasiEmail/verifikasiEmail'))

WebUI.verifyElementText(findTestObject('Object Punya Ardel/Website/PageVerifikasiEmail/verifikasiEmail'), 'Verifikasi Email')

