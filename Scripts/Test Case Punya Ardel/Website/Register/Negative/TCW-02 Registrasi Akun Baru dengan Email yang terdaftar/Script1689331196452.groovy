import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('https://demo-app.online/')

WebUI.click(findTestObject('Object Punya Ardel/Website/Page_Coding.ID/buttonBuatAkun'))

WebUI.setText(findTestObject('Object Punya Ardel/Website/PageBuatAkun/inputNama'), 'Nanami Kento')

WebUI.setText(findTestObject('Object Punya Ardel/Website/PageBuatAkun/inputTanggalLahir'), '12-Sep-2002')

WebUI.setText(findTestObject('Object Punya Ardel/Website/PageBuatAkun/inputEmail'), 'taksirey@gmail.com')

WebUI.setText(findTestObject('Object Punya Ardel/Website/PageBuatAkun/inputWhatsapp'), '09876543211')

WebUI.setEncryptedText(findTestObject('Object Punya Ardel/Website/PageBuatAkun/inputKataSandi'), 'GOgNTWnoDNKfJ0ckKDVxag==')

WebUI.setEncryptedText(findTestObject('Object Punya Ardel/Website/PageBuatAkun/inputKonfirmasiSandi'), 'GOgNTWnoDNKfJ0ckKDVxag==')

WebUI.click(findTestObject('Object Punya Ardel/Website/PageBuatAkun/inputCheckListSyaratKetentuan'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Punya Ardel/Website/PageBuatAkun/buttonDaftar'))

WebUI.verifyElementText(findTestObject('Object Punya Ardel/Website/PageBuatAkun/alertBuatAkun'), 'Email sudah terdaftar.')

WebUI.delay(5)

