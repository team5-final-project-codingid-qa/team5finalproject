import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration

Mobile.startApplication(RunConfiguration.getProjectDir() + '/Apk/DemoAppV2.apk', true)

Mobile.tap(findTestObject('Object Repository/Object Punya Ardel/Mobile/HomePage/buttonGoingToLoginPage'), 0)

for (int rowData = 1; rowData <= findTestData('Data Files/Data Login Mobile').getRowNumbers(); rowData++) {
    if ((findTestData('Data Files/Data Login Mobile').getValue(1, rowData) == 'ardeliayunisa23@gmail.com') && (findTestData(
        'Data Files/Data Login Mobile').getValue(2, rowData) == 'yunisa23')) {
        Mobile.clearText(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/inputEmail'), 0)

        Mobile.clearText(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/inputPassword'), 0)

        Mobile.tap(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/tapFieldEmail'), 0)

        Mobile.sendKeys(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/inputEmail'), findTestData(
                'Data Files/Data Login Mobile').getValue(1, rowData))

        Mobile.tap(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/tapFieldPassword'), 0)

        Mobile.sendKeys(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/inputPassword'), findTestData(
                'Data Files/Data Login Mobile').getValue(2, rowData))

        Mobile.tap(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/buttonLogin'), 0)

        Mobile.verifyElementExist(findTestObject('Object Repository/Object Punya Ardel/Mobile/HomePage/buttonCart'), 0)
    } else if (findTestData('Data Files/Data Login Mobile').getValue(1, rowData) == 'marianasir@gmail.com') {
        Mobile.clearText(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/inputEmail'), 0)

        Mobile.clearText(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/inputPassword'), 0)

        Mobile.tap(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/tapFieldEmail'), 0)

        Mobile.sendKeys(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/inputEmail'), findTestData(
                'Data Files/Data Login Mobile').getValue(1, rowData))

        Mobile.tap(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/tapFieldPassword'), 0)

        Mobile.sendKeys(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/inputPassword'), findTestData(
                'Data Files/Data Login Mobile').getValue(2, rowData))

        Mobile.tap(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/buttonLogin'), 0)

        Mobile.verifyElementExist(findTestObject('Object Punya Ardel/Mobile/LoginPage/emailNotVerified'), 0)

        Mobile.tap(findTestObject('Object Punya Ardel/Mobile/LoginPage/buttonBatalNotVerifiedEmail'), 0)
    } else if (findTestData('Data Files/Data Login Mobile').getValue(1, rowData) == '') {
        Mobile.tap(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/buttonLogin'), 0)

        Mobile.verifyElementExist(findTestObject('Object Punya Ardel/Mobile/LoginPage/invalidCredential'), 0)

        Mobile.tap(findTestObject('Object Punya Ardel/Mobile/LoginPage/buttonOkInvalidCredential'), 0)
    } else {
        Mobile.clearText(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/inputEmail'), 0)

        Mobile.clearText(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/inputPassword'), 0)

        Mobile.tap(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/tapFieldEmail'), 0)

        Mobile.sendKeys(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/inputEmail'), findTestData(
                'Data Files/Data Login Mobile').getValue(1, rowData))

        Mobile.tap(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/tapFieldPassword'), 0)

        Mobile.sendKeys(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/inputPassword'), findTestData(
                'Data Files/Data Login Mobile').getValue(2, rowData))

        Mobile.tap(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/buttonLogin'), 0)

        Mobile.verifyElementExist(findTestObject('Object Punya Ardel/Mobile/LoginPage/invalidCredential'), 0)

        Mobile.tap(findTestObject('Object Punya Ardel/Mobile/LoginPage/buttonOkInvalidCredential'), 0)
    }
}

