import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.configuration.RunConfiguration

Mobile.startApplication(RunConfiguration.getProjectDir() + '/Apk/DemoAppV2.apk', true)

Mobile.tap(findTestObject('Object Repository/Object Punya Ardel/Mobile/HomePage/buttonGoingToLoginPage'), 0)

if ((username == 'ardeliayunisa23@gmail.com') && (password == 'yunisa23')) {
    Mobile.tap(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/tapFieldEmail'), 0)

    Mobile.setText(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/inputEmail'), username, 0)

    Mobile.tap(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/tapFieldPassword'), 0)

    Mobile.setText(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/inputPassword'), password, 0)

    Mobile.tap(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/buttonLogin'), 0)

    Mobile.verifyElementExist(findTestObject('Object Repository/Object Punya Ardel/Mobile/HomePage/buttonCart'), 0) //	Mobile.tap(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/tapFieldEmail'), 0)
    //
    //	Mobile.setText(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/inputEmail'), username,0)
    //
    //	Mobile.tap(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/tapFieldPassword'), 0)
    //
    //	Mobile.setText(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/inputPassword'), password,0)
} else if (username == 'marianasir@gmail.com') {
    Mobile.tap(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/tapFieldEmail'), 0)

    Mobile.setText(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/inputEmail'), username, 0)

    Mobile.tap(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/tapFieldPassword'), 0)

    Mobile.setText(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/inputPassword'), password, 0)

    Mobile.tap(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/buttonLogin'), 0)

    Mobile.verifyElementExist(findTestObject('Object Punya Ardel/Mobile/LoginPage/emailNotVerified'), 0)

    Mobile.tap(findTestObject('Object Punya Ardel/Mobile/LoginPage/buttonBatalNotVerifiedEmail'), 0)
} else if ((username == 'ardeliayunisa23@gmail.com') || (username == 'ardelia@gmail.com')) {
    Mobile.tap(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/tapFieldEmail'), 0)

    Mobile.setText(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/inputEmail'), username, 0)

    Mobile.tap(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/tapFieldPassword'), 0)

    Mobile.setText(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/inputPassword'), password, 0)

    Mobile.tap(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/buttonLogin'), 0)

    Mobile.verifyElementExist(findTestObject('Object Punya Ardel/Mobile/LoginPage/invalidCredential'), 0)

    Mobile.tap(findTestObject('Object Punya Ardel/Mobile/LoginPage/buttonOkInvalidCredential'), 0)
} else {
    Mobile.tap(findTestObject('Object Repository/Object Punya Ardel/Mobile/LoginPage/buttonLogin'), 0)

    Mobile.verifyElementExist(findTestObject('Object Punya Ardel/Mobile/LoginPage/invalidCredential'), 0)

    Mobile.tap(findTestObject('Object Punya Ardel/Mobile/LoginPage/buttonOkInvalidCredential'), 0)
}

